#ifndef _PROC2_
#define _PROC2_

#include "systemc.h"

#include "channel_interface.h"
#include "printer.h"

SC_MODULE(proc2) {

    sc_port<channel_interface> dataPort;
	sc_in<bool> clockInputSignal;

    SC_CTOR(proc2) {
        SC_THREAD(program6);
        sensitive << clockInputSignal.pos();
    }

    void program6(){
        // printInfoStartProgram(6);
        while (true) {
            if (!dataPort->isWriteMode()&& dataPort->_top() == 6 && !dataPort->error){
                printInfoStartProgram(6);
            }
            wait();
        }
    }
};

#endif