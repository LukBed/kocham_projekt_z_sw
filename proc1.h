#ifndef _PROC1_
#define _PROC1_

#include "systemc.h"

#include "channel_interface.h"
#include "printer.h"

SC_MODULE(proc1) {
    
    sc_port<channel_interface> dataPort;
	sc_in<bool> clockInputSignal;

    SC_CTOR(proc1) {
        SC_THREAD(programmer);
        sensitive << clockInputSignal.pos();

        SC_THREAD(program1);
        sensitive << clockInputSignal.pos();
        
        SC_THREAD(program2);
        sensitive << clockInputSignal.pos();

        SC_THREAD(program3);
         sensitive << clockInputSignal.pos();
         
        SC_THREAD(program4);
        sensitive << clockInputSignal.pos();

        SC_THREAD(program5);
         sensitive << clockInputSignal.pos();
    }

    void programmer(){
        unsigned int userInput;

        system("clear");
        cout << "Proszę wybrać program (numery 1-6):"<<endl;
        while (true) {
            cin >> userInput;

            if (userInput > 6 || userInput < 1){
                system("clear");
                cout << "Proszę wybrać program (numery 1-6):"<<endl;
            } else {
                // system("clear");
                if (dataPort->isWriteMode()){
                    dataPort->writeSwitchState(userInput);
                } else {
                    if (userInput == dataPort->_top()){
                        dataPort->error = false;
                        cout << "Turn off (" << dataPort->readSwitchState() << ")" <<endl;
                    } else {
                        dataPort->error = true;
                        cout << "Cannot perform this action" <<endl;
                    }
                }
            }
            wait();
        }
    }

    void program1(){
        // printInfoStartProgram(1);
        while (true) {
            wait();
            if (!dataPort->isWriteMode() && dataPort->_top() == 1 && !dataPort->error){
                printInfoStartProgram(1);
            }
        }
    }

    void program2(){
        // printInfoStartProgram(2);
        while (true) {
            wait();
            if (!dataPort->isWriteMode()&& dataPort->_top() == 2 && !dataPort->error){
                printInfoStartProgram(2);
            }
        }
    }

    void program3(){
        // printInfoStartProgram(3);
        while (true) {
            wait();
            if (!dataPort->isWriteMode() && dataPort->_top() ==  3 && !dataPort->error){
                printInfoStartProgram(3);
            }
        }
    }

    void program4(){
        // printInfoStartProgram(4);
        while (true) {
            wait();
            if (!dataPort->isWriteMode()&& dataPort->_top() == 4 && !dataPort->error){
                printInfoStartProgram(4);
            }
        }
    }

    void program5(){
        // printInfoStartProgram(5);
        while (true) {
            wait();
            if (!dataPort->isWriteMode() && dataPort->_top() == 5 && !dataPort->error){
                printInfoStartProgram(5);
            }
        }
    }
};

#endif